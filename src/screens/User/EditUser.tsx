import React, { useEffect, useState } from 'react';
import UserForm from '../../components/UserForm';

const EditUser = (props: any) => {
    const [userId, setuserId] = useState(null);
    useEffect(() => {
        const id = props.match.params.id;
        setuserId(id);
    });

    return (
        <div>
            <h3>Edit User</h3>
            <a href="/users">Back</a>
            <UserForm id={userId}></UserForm>
        </div>
    );
}

export default EditUser;