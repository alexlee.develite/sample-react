import React, { useEffect } from 'react';
import UserForm from '../../components/UserForm';

const AddUser = () => {

    return (
        <div>
            <h3>Add User</h3>
            <a href="/users">Back</a>
            <UserForm></UserForm>
        </div>
    );
}

export default AddUser;