import React, { useEffect, useState } from 'react';
import { UserService } from './../../services/UserService';
import axios from 'axios';
import { Redirect } from 'react-router';
import { useHistory, withRouter } from 'react-router-dom';

const Users = (props: any) => {
    const [users, setUsers] = useState([]);
    const history = useHistory();
    

    useEffect(() => {
        getUsers();


        // axios.get('http://localhost:8081/api/users').then((res: any) => {
        //     setUsers(res.data);
        // }).catch((error: any) => {
        //     alert('Error fetching user list: ' + error);
        // });
        // UserService.getUserList().then((data: []) => {
        //     setUsers(data);
        // });
    }, [history.push]);

    // const getUserList = () => {
    //     console.log('getUserList');
    //     axios.get('http://localhost:8081/api/users').then((res: any) => {
    //         setUsers(res.data);
    //     }).catch((error: any) => {
    //         alert('Error fetching user list: ' + error);
    //     });
    // }

    const getUsers = async() => {
        const users = await UserService.getUserList();
        setUsers(users);
    }


    const confirmDelete = ($event: any, userId: String) => {
        $event.preventDefault();
        var isDelete = window.confirm("Confirm delete?");
        if (isDelete) {
            console.log('is deletee');
            UserService.deleteUser(userId).then(() => {
                // refresh list
                UserService.getUserList().then((response: any) => {
                    setUsers(response.data);
                });
            });


        } else {
            console.log('not delete');
        }
    }

    const toEditPage = ($event: any, userId: string) => {
        $event.preventDefault();

        console.log('userId: ' + userId);

        const url = `/users/${userId}/edit`;
        history.push(url);
    }

    return (
        <div>
            <h3>Users ({users.length})</h3>
            <a href="/users/add">Add user</a>
            <table>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {users && users.length ? users.map((user: any, index: number) => {
                        return <tr key={index}>
                            <td>{user.name}</td>
                            <td>{user.email}</td>
                            <td><a href="#" onClick={($event) => toEditPage($event, user._id)}>Edit</a>&nbsp;<a href="#" onClick={($e) => confirmDelete($e, user._id)}>Delete</a></td>
                        </tr>
                    }) : <tr><td colSpan={3}>No Data</td></tr>
                    }
                </tbody>
            </table>
        </div>
    );
}

export default withRouter(Users);