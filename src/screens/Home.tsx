import React, { useEffect } from 'react';

const Home = () => {

    return (
        <div>
            <div>Home</div>
            <div>
                <h3>Menu</h3>
                <ul>
                    <li>
                        <a href="/users">Users</a>
                    </li>
                </ul>
            </div>
        </div>
    );
}

export default Home;