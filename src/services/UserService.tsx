import React from 'react';
import axios from 'axios';

export const UserService = {
    getUserList,
    createUser,
    getUser,
    updateUser,
    deleteUser,
}

async function getUserList() {
    const response = await axios.get('http://localhost:8081/api/users');
    return response.data;
}

async function createUser(data: any) {
    const response = await axios.post('http://localhost:8081/api/users', data); 
}


async function updateUser(userId: String, data: any) {
    axios.post(`http://localhost:8081/api/users/${userId}`, data)
        .then(response => {
            console.log(response.data);
            return response.data;
        })
        .catch(error => {
            console.log(error);
        });
}


async function deleteUser(userId: String) {
    return axios.delete(`http://localhost:8081/api/users/${userId}`);
        // .then(response => {
        //     console.log(response.data);
        //     return response.data;
        // })
        // .catch(error => {
        //     console.log(error);
        // });
}

async function getUser(userId: String) {
    const response = await axios.get(`http://localhost:8081/api/users/${userId}`);
    if (response.data.length > 0) {
        return response.data[0];
    }
    return null;
}
