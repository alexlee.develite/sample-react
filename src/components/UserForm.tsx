import React, { useEffect, useState } from 'react';
import { useForm } from "react-hook-form";
import { UserService } from '../services/UserService';
import { useHistory, withRouter } from 'react-router-dom';
import axios from 'axios';

type FormData = {
    name: string;
    email: string;
};

const UserForm = (props: any) => {
    const { register, setValue, handleSubmit, errors, reset } = useForm<FormData>();
    const [formMode, setformMode] = useState("add");
    const [userId, setUserId] = useState("");
    const history = useHistory();

    useEffect(() => {
        if (props.id != null) {
            setUserId(props.id);
            setformMode("edit");
            getUser(props.id);
        }
    }, [props.id, userId, reset]);

    const onSubmit = handleSubmit(({ name, email }) => {
        console.log(name, email);
        const createUserReq = { name, email };
        saveUser(createUserReq);
    });

    const getUser = async (userId: String) => {
        const user = await UserService.getUser(userId);
        reset(user);
    }

    const saveUser = async (createUserReq: any)  => {
        if (formMode === "add") {
            await UserService.createUser(createUserReq);
            history.push("/users");
        } else {
            await UserService.updateUser(userId, createUserReq);
            history.push("/users");
        }
    }

    return (
        <div>
            <h3>Form</h3>
            <form onSubmit={onSubmit}>
                <label htmlFor="name">Name</label>
                <input type="text" name="name" id="name"
                    ref={register({
                        required: true,
                        pattern: /^[A-Za-z]+$/i
                    })} />
                {errors.name && "<p>Name is required</p>"}
                <label htmlFor="email">Email</label>
                <input type="text" name="email" id="email" ref={register({ required: true })} />
                {errors.email && "<p>Email is required</p>"}
                <button type="submit">Submit</button>
            </form>
        </div>
    );
}

export default withRouter(UserForm);