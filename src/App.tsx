import React from 'react';
import logo from './logo.svg';
import './App.css';
import { Router, Route, Switch } from 'react-router';
import { createBrowserHistory } from 'history';

import Home from './screens/Home';
import Users from './screens/User/Users';
import AddUser from './screens/User/AddUser';
import EditUser from './screens/User/EditUser';

const history = createBrowserHistory();

function App() {
  return (
    <Router history={history}>
      <Switch>
        <Route exact path='/' component={Home} />
        <Route path='/users/:id/edit' component={EditUser} />
        <Route path='/users/add' component={AddUser} />
        <Route path='/users' component={Users} />
      </Switch>
    </Router >
  );
}

export default App;
